#!/bin/sh
#
# Use BACKPORT=yes to have "gbp dch --bpo"
#

#set -x
set -e

set_upstream_remote () {
  #test -d .git/refs/remotes/mpd_github/ || git remote add mpd_github https://github.com/MusicPlayerDaemon/MPD.git
  grep -q 'remote "mpd_github"' .git/config || { \
    git remote add mpd_github https://github.com/MusicPlayerDaemon/MPD.git; \
    git fetch --depth=100 --quiet mpd_github; \
  }
  git fetch --quiet mpd_github
}

chk_watch_file () {
  grep -q 'mode=git, pretty=describe' ./debian/watch && return 0
  echo 'watch file not set to fetch from git'
  set_watch_file
}

set_watch_file () {
  cat <<EOF > ./debian/watch
version=4
opts="mode=git, pretty=describe, pgpmode=none, uversionmangle=s/^v//" \
https://github.com/MusicPlayerDaemon/MPD.git \
heads/master debian
EOF
  git commit -q -m"Update watch file to fetch from git" -m'Gbp-Dch: Ignore' ./debian/watch
}

fetch_upstream () {
  echo "Merging upstream changes"
  gbp import-orig --no-pristine-tar --uscan --no-interactive
  #PKG_VERSION=$(git log -1 --pretty=format:%s upstream | grep -Po '(?<=New upstream version ).*')
  PKG_VERSION=$(git describe --abbrev=9 mpd_github/master | sed s/-/./g | sed s/^v//)
  echo "    tagging ${PKG_VERSION} debian/changelog"
  gbp dch ${BACKPORT:+--bpo} --spawn-editor=never -aR --git-author --new-version ${PKG_VERSION}-1
  git commit -q debian/changelog -m"Update changelog (${PKG_VERSION})" -m"Gbp-Dch: Ignore"
  git tag debian/${PKG_VERSION}-1${BACKPORT:+~bpo11+1}
}

if [ $# -eq 0 ]
then
  chk_watch_file
  set_upstream_remote
  fetch_upstream
else
  $@
fi

# vim: fileencoding=utf8
